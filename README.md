# SR04 Groupe 12

Bimont Candice
Gillet Guillaume
Chin Juliette
Giramelli Manon

## Backend de la plateforme de gestion multi cloud

# Mise en place des Environements
Vous devez avoir un environement avec nodejs instaler
* installer les dependances: 
```
npm install
```
 
## Environement de dévelopements
* Lancer le backend:
```
node index.js
```

## Environemment de production
Modifier les fichiers ./index.js pour adapter la constante ip avec votre ip.
affin d'assurer la maintenabiliter de la plateforme nous vous conseillon d'utiliser le superviseur pm2:
```
npm install pm2 -g
pm2 start index.js
pm2 startup # suivre les instruction
pm2 save
```
