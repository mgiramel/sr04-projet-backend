const express = require('express');
const multer = require('multer');
const crypto = require('crypto');
var secrets = require('secret-sharing.js');//sharmir
const app = express(); //app:instanciation objet Express, qui contient notre serveur et methodes necessaires
const path = require('path');
const fs = require('fs');
const cpFile = require('cp-file');
const fichiers = require('./fichiers.json')
const ip="localhost"
var filesTab = []
// si un fichier iv existe en recuperent le vecteur existant
function getIv(){
    let tmpiv 
    if (!fs.existsSync(path.join(__dirname,"iv"))){
        tmpiv = crypto.randomBytes(16)
        fs.writeFileSync(path.join(__dirname,"iv"), tmpiv.toString("hex") , function(err) {
            if(err) {
                return console.log(err);
            }
        }); 
    }else{
        try {
            let ivHex = fs.readFileSync(path.join(__dirname,"iv")).toString()
            tmpiv= Buffer.from(ivHex, "hex")       
        } catch (err) {
            return console.error(err)
        }
    }
    return tmpiv
}
//definition du vecteru initial pour crypto
const iv =getIv()

//Lecture contenu du dossier dir1

const directoryPath = path.join(__dirname, 'Dir1');
const directoryPath2 = path.join(__dirname, 'Dir2');
const directoryPath3 = path.join(__dirname, 'Dir3');
const directoryPathDec = path.join(__dirname, 'DirDec');
if (!fs.existsSync(directoryPath)){

    fs.mkdirSync(directoryPath);
}
if (!fs.existsSync(directoryPath2)){

    fs.mkdirSync(directoryPath2);
}
if (!fs.existsSync(directoryPath3)){

    fs.mkdirSync(directoryPath3);
}
if (!fs.existsSync(directoryPathDec)){

    fs.mkdirSync(directoryPathDec);
}

//definition du dossier de stockage par defaut
var storage = multer.diskStorage(
    {
        destination: 'Dir1/',
        filename: function (req, file, cb) {
            cb(null, file.originalname); //on ne change pas le nom du fichier en le mettant dans Dir1
        }
    }
);
// l'uploade de fichier sera par defaut effectuer dans storage
var upload = multer({ storage: storage });


// Middleware: Pour récupérer les données passées dans la requête POST; interpréte le body de la requête
// Ce middleware va se placer entre l'arrivée de la requête et nos routes et exécuter son code, rendant possible l'accès au body.
app.use(express.json())
// Add headers before the routes are defined
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

//Récupérer la liste des fichiers
//.get definit une route GET, 1er param:String definit la route à ecouter, 2e: callback càd fonction à executer si cette route est appelee. 
//parametres callback: l'objet req (toutes les donnees fournies par la requête) ; l'objet res contient methodes pour repondre à la requête
app.get('/fichiers', (req, res) => {
    let listFile = []
    try{
        let files = fs.readdirSync(directoryPath)
        //handling error
        
        var i = 0;
        
        //listing all files using forEach
        files.forEach(function (file) {
            // Do whatever you want to do with the file
            //exemple test.txt.enc
            let ext = file.substring(file.length - 4); // =>".enc" ou ".key"
            //ext=.enc
            let filename = file.slice (0,file.length - 4 ) ;
            //filename =test.txt
            if (ext == ".enc"){
                listFile.push(filename)
            }
            filesTab[i] = file
            i += 1
            console.log(file)
            

        });
        res.status(200).send(listFile)  //statut 200: code réponse http indiquant au client que sa requête s'est terminée avec succès.
    } catch  (err) {
        console.error(err);
    }
    
});

//Récupérer les info sur le dossier
app.get('/fichiers/info/', (req, res) => {
    let info ={
        nbFile : 0,
        size : 0,
    };
    try {
        let files = fs.readdirSync(directoryPath);
        info.nbFile = Math.floor(files.length/2)
        //console.log(files.length);
        files.forEach(file => {
            try {
                let stats = fs.statSync(path.join(directoryPath,file));
                //console.log(stats.size)
                info.size += stats.size
            } catch (err) {
                console.error(err);
            }
           
        })
    }catch  (err) {
        console.error(err);
    }
    
    console.log(info)
    res.status(200).json(info);
    
});
// reconstruit le dossier d'id forunit a partir des deux dossier restant
app.get('/fichiers/restore/:id', (req, res) => {
    const id = req.params.id;
    let listFile =[]
    let directoryPathi = path.join(__dirname, 'Dir'+id);
    try {
        let directoryPathUp = directoryPath;
        if (id == "1"){
            directoryPathUp = directoryPath2
        }
        let files = fs.readdirSync(directoryPathUp);
        // on parcourt tout les fichier
        files.forEach(file => {
            
            let ext = file.substring(file.length - 4); // =>".enc" ou ".key"
            let filename = file.slice (0,file.length - 4 ) ;
            if (ext == ".enc"){
                listFile.push(filename)
                // on ajoute le fichier chifrer au dossier
                cpFile.sync(path.join(directoryPathUp, filename+".enc"), path.join(directoryPathi, filename+".enc"));
               
                let shares = []
                // on recupère les deux parties de clé
                for (var i = 0; i < 3; i++) {
                    let dir =i+1;
                    if (dir.toString() != id){
                        let directoryPathi = path.join(__dirname, 'Dir'+dir.toString());
                        try {
                            const data = fs.readFileSync(path.join(directoryPathi, filename+".key")).toString()
                            //console.log(data)
                            shares.push(data)
                            
                        } catch (err) {
                            console.error(err)
                        }
                    }
                 };
                 // on crée un nouveaux morceau de clé compatible avec les deux autres
                 let share = secrets.newShare( id, shares );
                 // on enregistre ce morceau de clé
                 fs.writeFileSync(path.join(directoryPathi, filename+".key"), share , function(err) {
                    if(err) {
                        return console.log(err);
                    }
                
                    console.log("The file was saved!");
                }); 
            }
           
        })
    }catch  (err) {
        console.error(err);
    }
    res.status(200).json(listFile);
    
});
//Récupérer un fichier via son nom
app.get('/fichiers/:name', (req, res) => {
    const name = req.params.name
    //console.log(name)
    if (!fs.existsSync(path.join(directoryPath, name+".enc"))){
        res.status(404).json();
    }else{
        //Dechifrage
        //console.log(secrets.combine( shares ))
        let shares = []
        // on recupère deux morceaux de clé
        for (var i = 0; i < 2; i++) {
            let dir =i+1;
            let directoryPathi = path.join(__dirname, 'Dir'+dir.toString());
            try {
                const data = fs.readFileSync(path.join(directoryPathi, name+".key")).toString()
                //console.log(data)
                shares.push(data)
                
            } catch (err) {
                console.error(err)
            }
            
          
           
        };
        // on en deduit la clé
        let key = Buffer.from(secrets.combine( shares ) , "hex")
        let decipher = crypto.createDecipheriv('aes-256-cbc', key, iv)
        let inputenc = fs.createReadStream(path.join(directoryPath, name+".enc"));
        let outputenc = fs.createWriteStream(path.join(directoryPathDec, name)); 
        // on dechifre le fichier et on le met a disposition dans le dossier DirDec
        inputenc.pipe(decipher).pipe(outputenc).on('finish', function () {
            res.download(path.join(directoryPathDec, name));
            setTimeout(function() {
                deleteDec(name);
            }, 10000);
        });
       
    }
});

function deleteDec(name){
    console.log("timeout")
    if (!fs.existsSync(path.join(directoryPathDec, name))){
        console.log("fichier a suprimer inexsitant");
    }else{
        try{
       
            fs.unlinkSync(path.join(directoryPathDec, name));
    
        
        }catch(err){
            console.error(err);
        }
    }
}
//Ajouter un fichier dans la liste 
// app.post('/fichiers', (req,res) => {
//     console.log(req.fil)
//     // filesTab.push(req.name)
//     // res.status(200).send(filesTab)
// })

function dechifrement (filename) {
    //Dechifrage
        //console.log(secrets.combine( shares ))
        let shares = []
        // on recupère deux morceaux de clé
        for (var i = 0; i < 2; i++) {
            let dir =i+1;
            let directoryPathi = path.join(__dirname, 'Dir'+dir.toString());
            try {
                const data = fs.readFileSync(path.join(directoryPathi, filename+".key")).toString()
                //console.log(data)
                shares.push(data)
                
            } catch (err) {
                console.error(err)
            }
            
          
           
        };
        // on en deduit la clé
        let key = Buffer.from(secrets.combine( shares ) , "hex")
        let decipher = crypto.createDecipheriv('aes-256-cbc', key, iv)
        let inputenc = fs.createReadStream(path.join(directoryPath, filename+".enc"));
        let outputenc = fs.createWriteStream(path.join(directoryPathDec, filename)); 
        // on dechifre le fichier et on le met a disposition dans le dossier DirDec
        inputenc.pipe(decipher).pipe(outputenc)
};

app.post('/fichiers', upload.single('file'), function (req, res, next) {
    
    // Le fichier a été recus et enregistrer dans dir1
    // Mettre ici le traitement et la copie
    //console.log(crypto.randomBytes(32).toString('hex'))
    // transforme key en hexa pour sharmir
    let keyHex = crypto.randomBytes(32).toString('hex')
    //console.log(keyHex)
    let key = Buffer.from(keyHex, "hex")
    let cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
    let input = fs.createReadStream(path.join(directoryPath, req.file.originalname));
    let output = fs.createWriteStream(path.join(directoryPath, req.file.originalname+".enc"));
    // on chiffre le fichier
    input.pipe(cipher).pipe(output).on('finish', function () { // quand le chiffrement est fini
        // on suprime le fichier en claire
        fs.unlinkSync(path.join(directoryPath, req.file.originalname))
        // on crée 3 morceaux de clé suivent l'agorithme de sharmir
        let shares = secrets.share( keyHex, 3, 2);
        // on enregistre les morceaux
        for (var i = 0; i < 3; i++) {
            let dir =i+1;
            let directoryPathi = path.join(__dirname, 'Dir'+dir.toString())
            //console.log(shares[i])
            fs.writeFileSync(path.join(directoryPathi, req.file.originalname+".key"), shares[i] , function(err) {
                if(err) {
                    return console.log(err);
                }
            
                console.log("The file was saved!");
            }); 
        
        };
        
       // on copie l fichier chiffrer
        cpFile.sync(path.join(directoryPath, req.file.originalname+".enc"), path.join(directoryPath2, req.file.originalname+".enc"));
        cpFile.sync(path.join(directoryPath, req.file.originalname+".enc"), path.join(directoryPath3, req.file.originalname+".enc"));

    })
    

    // req.file is the `avatar` file
    // req.body will hold the text fields, if there were any
    res.status(200).json("")

})



//Supprimer un fichier

app.delete('/fichiers/:name', (req, res) => {
    const name = req.params.name
    //console.log(name)
    if (!fs.existsSync(path.join(directoryPath, name+".enc"))){
        res.status(404).json();
    }else{
        try{
            // on suprimme les copies de fichier et les morceau de clé
            for (var i = 0; i < 3; i++) {
                let dir =i+1;
                let directoryPathi = path.join(__dirname, 'Dir'+dir.toString());
                fs.unlinkSync(path.join(directoryPathi, name+".enc"));
                fs.unlinkSync(path.join(directoryPathi, name+".key"));
            }
            
        }catch(err){
            console.error(err);
        }
        
        res.status(200).json("")
    }
});
app.listen(8010, () => {
    
    console.log("Serveur à l'écoute à l'adresse http://"+ip+":8010/")
}) 